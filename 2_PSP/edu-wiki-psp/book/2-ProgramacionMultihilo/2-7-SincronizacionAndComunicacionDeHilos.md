# Sincronización y Comunicación de Hilos

Los ejemplos realizados hasta ahora utilizan hilos independientes; una vez iniciados los hilos, éstos no se relacionan con los demás y no acceden a los mismos datos u objetos, por lo que no hay conflictos entre ellos. Sin embargo, hay ocasiones en las que distintos hilos de un programa necesitan establecer alguna relación entre sí y **compartir recursos o información**. Se pueden presentar las siguientes situaciones:

* Dos o más hilos **compiten por obtener un mismo recurso**, por ejemplo dos hilos que quieren escribir en un *mismo fichero* o acceder a la *misma variable* para modificarla.

* Dos o más hilos **colaboran para obtener un fin común** y para ello, necesitan comunicarse a través de algún recurso. Por ejemplo un hilo produce información que utilizará otro hilo.

En cualquiera de estas situaciones, es necesario que los hilos se ejecuten de manera controlada y coordinada, para evitar posibles interferencias que pueden desembocar en programas que se bloquean con facilidad y que intercambian datos de manera equivocada.

**¿Cómo conseguimos que los hilos se ejecuten de manera coordinada?** Utilizando sincronización y comunicación de hilos:

* **Sincronización**. Es la capacidad de informar de la situación de un hilo a otro. El objetivo es establecer la secuencialidad correcta del programa.

* **Comunicación**. Es la capacidad de transmitir información desde un hilo a otro. El objetivo es el intercambio de información entre hilos para operar de forma coordinada.

**En Java la sincronización y comunicación de hilos se consigue mediante:**

* **Monitores**. Se crean al marcar bloques de código con la palabra **`synchronized`**.

* **Semáforos**. Podemos implementar nuestros propios   semáforos, o bien utilizar la clase `Semaphore` incluida en el paquete `java.util.concurrent`.

* **Notificaciones**. Permiten comunicar hilos mediante los métodos `wait()`, `notify()` y `notifyAll()` de la clase `java.lang.Object`.

Por otra parte, Java proporciona en el paquete `java.util.concurrent` varias clases de sincronización que permiten la sincronización y comunicación entre diferentes hilos de una *aplicación multithreadring*, como son: `Semaphore`, `CountDownLatch`, `CyclicBarrier` y `Exchanger`.

En los siguientes apartados veremos ejemplos de todo esto.

## Información compartida entre hilos

**Las secciones críticas** son aquellas secciones de código que no pueden ejecutarse concurrentemente, pues en ellas se encuentran los recursos o información que comparten diferentes hilos, y que por tanto pueden ser problemáticas.

![Ilustración de Información compartida entre hilos](img/InformacionCompartidaEntreHilos.png)

Un ejemplo sencillo que ilustra lo que puede ocurrir cuando varios
hilos actualizan una misma variable es el clásico "*ejemplo de los jardines*". En él, se pone de manifiesto el problema conocido como la "*condición de carrera*", que se produce cuando varios hilos acceden a la vez a un mismo recurso, por ejemplo a una variable, cambiando su valor y obteniendo de esta forma un valor no esperado de la misma. En el siguiente enlace te facilitamos este ejemplo detallado.

![](doc/PSP02_CONT_R45_NecesidadSincronizarHilos/captura.png) 
[SWF](doc/PSP02_CONT_R45_NecesidadSincronizarHilos/flash.swf) [HTML](doc/PSP02_CONT_R45_NecesidadSincronizarHilos/flash.html) [REPO](https://bitbucket.org/eduxunta/edu-java-psp-programacionmultihilo/src/samples-problemaJardines/problemaJardines/)

En el ejemplo del "*problema de los jardines*", el recurso que comparten diferentes hilos es la variable contador cuenta. Las secciones de código donde se opera sobre esa variable son dos secciones críticas, los métodos `incrementaCuenta()` y `decrementaCuenta()`.

La forma de proteger las secciones críticas es mediante sincronización. La sincronización se consigue mediante:

* **Exclusión mutua**. Asegurar que un hilo tiene acceso a la sección crítica de forma exclusiva y por un tiempo finito.

* **Por condición**. Asegurar que un hilo no progrese hasta que se cumpla una determinada condición.

**En Java, la sincronización para el acceso a recursos compartidos se basa en el concepto de monitor.**

## Monitores. Métodos `synchronized`

En Java, un monitor es una porción de código protegida por un *mutex* o *lock*. 

> Un mutex es un elemento que protege recursos con exclusión mutua.

Para crear un monitor en Java, hay que marcar un bloque de código con la palabra `synchronized`, pudiendo ser ese bloque:

* Un **método completo**.
* Cualquier **segmento de código**.

Añadir synchronized a un método significará que:

* Hemos creado un monitor asociado al objeto.
* Sólo un hilo puede ejecutar el método synchronized de ese objeto a
la vez.
* Los hilos que necesitan acceder a ese método `synchronized` permanecerán bloqueados y en espera. 
* Cuando el hilo finaliza la ejecución del método `synchronized`, los hilos en espera de poder ejecutarlo se desbloquearán. El planificador Java seleccionará a uno de ellos.

En [recurso adicional](doc/PSP02_CONT_R48_MonitoresJava.pdf) encontrarás una explicación detallada sobre el funcionamiento de un monitor Java.

Y **¿qué bloques interesa marcar como `synchronized`?** Precisamente los que se correspondan con secciones críticas y contengan el código o datos que comparten los hilos.

En el ejemplo anterior, "*Problema de los jardines*" se debería sincronizar tanto el método `incrementaCuenta()`, como el `decrementaCuenta()` tal y como ves en el siguiente código, ya que estos métodos contienen la variable cuenta, la cual es modificada por diferentes hilos. Así mientras un hilo ejecuta el método `incrementaCuenta()` del objeto de tipo `Jardin`, `jardin.incrementaCuenta()`, ningún otro hilo podrá ejecutarlo.

```java
/** Método que incrementa en 1 la variable cuenta */
public synchronized void incrementaCuenta(){
	System.out.println("hilo " + Thread.currentThread().getName() + "----- Entra en Jardín");
	// muestra el hilo que entra en el método
	cuenta++;
	// cuenta cada acceso al jardín y muestra el número de accesos
	System.out.println(cuenta +. " en jardín");
}
```
En el siguiente [recurso adicional (repositorio git)](https://bitbucket.org/eduxunta/edu-java-psp-programacionmultihilo/src/samples-problemaJardinesSincronizado/) dispones del proyecto completo con los dos métodos sincronizados.

En el siguiente recurso adicional (presentación y repositorio git)  dispones de otro sencillo ejemplo que simula el acceso simultáneo de 4 terminales a un servidor utilizando monitores Java.

![Captura del flash player](doc/PSP02_CONT_R51_SincronizarMetodo/captura.png) 
[SWF](doc/PSP02_CONT_R51_SincronizarMetodo/flash.swf) [HTML](doc/PSP02_CONT_R51_SincronizarMetodo/flash.html) [REPO](https://bitbucket.org/eduxunta/edu-java-psp-programacionmultihilo/src/samples-monitoresSynchronized/)

## Monitores. Segmentos de código `synchronized`

Hay casos en los que no se puede, o no interesa sincronizar un método. Por ejemplo, no podremos sincronizar un método que no hemos creado nosotros y que por tanto no podemos acceder a su código fuente para añadir `synchronized` en su definición. La forma de resolver esta situación es poner las llamadas a los métodos que se quieren sincronizar dentro de segmentos sincronizados de la siguiente forma: 

`synchronized (objeto){ // sentencias segmento; }`

En este caso el funcionamiento es el siguiente:

* **El objeto que se pasa al segmento**, es el objeto donde está el método que se quiere sincronizar.
* **Dentro del segmento se hará la llamada** al método que se quiere sincronizar.
* **El hilo que entra** en el segmento declarado `synchronized` se hará con el monitor del objeto, si está libre, o se bloqueará en espera de que quede libre. El monitor se libera al salir el hilo del segmento de código `synchronized`.
* **Sólo un hilo** puede ejecutar el segmento synchronized a la vez.

En el ejemplo del *problema de los jardines*, aplicando este procedimiento, habría que sincronizar el objeto que denominaremos jardin y que será desde donde se invoca al método `incrementaCuenta()`, método que manipula la variable cuenta que modifican diferentes hilos:

```java
/** Incrementa la cuenta de accesos*/
public void run(){
	for(int i=1; i<=10; i++) { // se simulan 10 accesos
		// segmento que se sincroniza
		synchronized(jardin){
			jardin.incrementaCuenta();
		}
	}
}
```

Observa que:

* Ahora el método `incrementaCuenta()` no será `synchronized` (se haría igual para `decrementaCuenta()`), 
* Se está **consiguiendo un acceso con exclusión mutua** sobre el objeto de tipo `Jardin`, **aún cuando su clase no contiene ningún segmento ni método `synchronized`**.

En el siguiente [recurso adicional](https://bitbucket.org/eduxunta/edu-java-psp-programacionmultihilo/src/samples-problemaJardinesSincronizaSegmento/) dispones del ejemplo completo con los cambios que acabamos de indicar.

Debes tener en cuenta que:

* Declarar un método o segmento de código como sincronizado ralentizará la ejecución del programa, ya que la **adquisición y liberación de monitores genera una sobrecarga**.
* Siempre que sea posible, por legibilidad del código, **es mejor sincronizar métodos completos**.
* Al declarar bloques synchronized puede aparecer un **nuevo problema**, denominado *interbloqueo* (lo veremos más adelante).

> Muchos métodos de las clases predefinidas de Java ya están sincronizados. Por ejemplo, el método de la clase `Component` de Java AWT que agrega un objeto `MouseListener` a un `Component` (para que `MouseEvents` se registren en el `MouseListener`) está sincronizado. Si compruebas el código fuente de AWT y Swing, encontrarás que el prototipo de este método es: 
> 
> `public synchronized void addMouseListener(MouseListener l)`

## Comunicación entre hilos con métodos de `java.lang.Object`

La comunicación entre hilos la podemos ver como un mecanismo de auto-sincronización, que consiste en logar que un hilo actúe solo cuando otro ha concluido cierta actividad (y viceversa).

Java soporta **comunicación entre hilos** mediante los siguientes métodos de la clase `java.lang.Object`.

* `wait()`. Detiene el hilo (pasa a "no ejecutable"), el cual no se reanudará hasta que otro hilo notifique que ha ocurrido lo esperado.

* `wait(long tiempo)`. Como el caso anterior, solo que ahora el hilo también puede reanudarse (pasar a "ejecutable·) si ha concluido el tiempo pasado como parámetro.

* `notify()`. Notifica a uno de los hilos puestos en espera para el mismo objeto, que ya puede continuar. 

* `notifyAll()`. Notifica a todos los hilos puestos en espera para el mismo objeto que ya pueden continuar.

> **La llamada a estos métodos se realiza dentro de bloques synchronized.**

**¿Cómo funcionan realmente estos métodos?** En el siguiente [recurso adicional](doc/PSP02_CONT_R57_ComunicacionHilos.pdf) tienes la explicación de cómo funcionan estos métodos y un ejemplo de su uso.

**Dos problemas clásicos** que permiten ilustrar la necesidad de sincronizar y comunicar hilos son:

* El problema del **Productor-Consumidor**. Del que has visto un ejemplo anteriormente y que permite modelar situaciones en las que se divide el trabajo entre los hilos. Modela el acceso simultáneo de varios hilos a una estructura de datos u otro recurso, de manera que unos hilos producen y almacenan los datos en el recurso y otros hilos (consumidores) se encargan de eliminar y procesar esos datos.
* El problema de los **Lectores-Escritores**. Permite modelar el acceso simultáneo de varios hilos a una base de datos, fichero u otro recurso, unos queriendo leer y otros escribir o modificar los datos.

En el siguiente recurso tienes el ejemplo del problema de los lectores-escritores, resuelto mediante los métodos `wait()` y `notify()` vistos anteriormente.

![Captura del flash player](doc/PSP02_CONT_R58_comunicacionHilos_LectoresEscritores/captura.png)

[SWF](doc/PSP02_CONT_R58_comunicacionHilos_LectoresEscritores/flash.swf) [HTML](doc/PSP02_CONT_R58_comunicacionHilos_LectoresEscritores/flash.html) [REPO](https://bitbucket.org/eduxunta/edu-java-psp-programacionmultihilo/src/samples-lectoresEscritores/)

## El problema del interbloqueo (deadlock)

El **interbloqueo** o bloqueo mutuo (deadlock) consiste en que uno a más hilos, **se bloquean o esperan indefinidamente**.

¿Cómo se llega a una situación de interbloqueo? A dicha situación se llega:

* Porque cada hilo **espera a que le llegue un aviso** de otro hilo **que nunca le llega**.

* Porque todos los **hilos, de forma circular, esperan para acceder a un recurso**.

El problema del bloqueo mutuo, en las aplicaciones concurrentes, se podrá dar fundamentalmente cuando un hilo entra en un bloque `synchronized`, y a su vez llama a otro bloque `synchronized`, o bien al utilizar clases de `java.util.concurrent` que llevan implícita la exclusión mutua.

En el siguiente [recurso adicional](https://bitbucket.org/eduxunta/edu-java-psp-programacionmultihilo/src/samples-interbloqueo/) encontrarás un ejemplo que produce interbloqueo. Observa que no finaliza la ejecución del programa. Habrá que finalizarlo manualmente.

Otro problema, menos frecuente, es **la inanición** (*starvation*), que consiste en que un hilo es desestimado para su ejecución. Se produce cuando un hilo no puede tener acceso regular a los recursos compartidos y no puede avanzar, quedando bloqueado. Esto puede ocurrir porque el hilo nunca es seleccionado para su procesamiento o bien porque otros hilos que compiten por el mismo recurso se lo impiden.

Está claro que los programas que desarrollemos deben estar exentos de estos problemas, por lo que habrá que ser cuidadosos en su diseño.

## La clase `Semaphore`

La clase `Semaphore` del paquete `java.util.concurrent`, permite definir un semáforo para controlar el acceso a un recurso compartido.

Para **crear y usar un objeto `Semaphore`** haremos lo siguiente:

* Indicar al constructor `Semaphore` (`int permisos`) el total de permisos que se pueden dar para acceder al mismo tiempo al recurso compartido. Este valor coincide con el número de hilos que pueden acceder a la vez al recurso. 

* Indicar al semáforo mediante el método `acquire()`, que queremos acceder al recurso, o bien mediante `acquire(int permisosAdquirir)` cuántos permisos se quieren consumir al mismo tiempo.

* Indicar al semáforo mediante el método `release()`, que libere el permiso, obien mediante `release(int permisosLiberar)`, cuantos permisos se quieren liberar al mismo tiempo.

* Hay otro constructor `Semaphore(int permisos, boolean justo)` que mediante el parámetro `justo` permite garantizar que el primer hilo en invocar `adquire()` será el primero en adquirir un permiso cuando sea liberado. Esto es, garantizar el orden de adquisición de permisos, según el orden en que se solicitan.

¿Desde dónde se deben invocar estos métodos? Esto dependerá del **uso de `Semaphore`**.

* Si se usa **para proteger secciones críticas**, la llamada a los métodos `acquire()` y `release()` se hará desde el recurso compartido o sección crítica, y el número de permisos pasado al constructor será 1.

* Si se usa **para comunicar hilos**, en este caso un hilo invocará al método `acquire()` y otro hilo invocará al método `release()` para así trabajar de manera coordinada. El número de permisos pasado al constructor coincidirá con el número máximo de hilos bloqueados en la cola o lista de espera para adquirir un permiso.

En el siguiente [recurso adicional `sampleSemaforo1`](https://bitbucket.org/eduxunta/edu-java-psp-programacionmultihilo/src/samples-semaforos/) tienes un ejemplo del uso de `Semaphore` para proteger secciones críticas o recursos compartidos. Es el ejemplo que vimos del acceso simultáneo de 4 terminales a un Servidor, pero resuelto ahora con la clase Semaphore en vez de con `synchronized`.

En el siguiente [recurso adicional `sampleSemaforo2`](https://bitbucket.org/eduxunta/edu-java-psp-programacionmultihilo/src/samples-semaforos/) tienes un ejemplo del uso de `Semaphore` para comunicar hilos. Es el ejemplo de los Lectores-Escritores. Se inician 5 hilos lectores y 2 escritores, como en el ejemplo que resolvimos con `wait()` y `notify()`.

## La clase `Exchanger`

La clase `Exchanger`, del paquete `java.util.concurrent`, establece un **punto de sincronización** donde se intercambian objetos entre dos hilos. La clase `Exchanger<V>` es genérica, lo que significa que tendrás que especificar en `<V>` el tipo de objeto a compartir entre los hilos.

Existen dos métodos definidos en esta clase: 

* `exchange(V x)`
* `exchange(V x, long timeout, TimeUnit unit)`

Ambos métodos `exchange()` permiten intercambiar objetos entre dos hilos. El hilo que desea obtener la información, esperará realizando una llamada al método `exchange()` hasta que el otro hilo sitúe la información utilizando el mismo método, o hasta que pase un periodo de tiempo establecido mediante el parámetro `timeout`.

![Ilustración `Exchanger`](img/Exchanger.png)

El funcionamiento, tal y como puedes apreciar en la imagen anterior, sería el siguiente:

* Dos hilos (`hiloA` e `hiloB`) intercambiarán objetos del mismo tipo, `objetoA` y `objetoB`.
* El `hiloA` invocará a `exchange(objetoA)` y el `hiloB` invocará a `exchange(objetoB)`.
* El hilo que procese su llamada a `exchange(objeto)` en primer lugar, se bloqueará y quedará a la espera de que lo haga el segundo. Cuando eso ocurra y se libere el bloqueo sobre ambos hilos, la salida del método `exchange(objetoA)` proporciona el objeto `objetoB` al `hiloA`, y la del método `exchange(objetoB)` el objeto `objetoA` al `hiloB`.

Te estarás preguntando ¿y **cuándo puede ser útil `Exchanger`**? Los *intercambiadores* se emplean típicamente cuando un hilo productor está rellenando una lista o búfer de datos, y otro hilo consumidor los está consumiendo.

De esta forma cuando el consumidor empieza a tratar la lista de datos entregados por el productor, el productor ya está produciendo una nueva lista. Precisamente, esta es **la principal utilidad de los intercambiadores: que la producción y el consumo de datos, puedan tener lugar concurrentemente**.

En el siguiente [recurso adicional `sampleExchanger`](https://bitbucket.org/eduxunta/edu-java-psp-programacionmultihilo/src/samples-Exchanger/) encontrarás un ejemplo donde un hilo productor se encarga de rellenar una cadena de diez caracteres (o sea, una lista de 10 caracteres) mientras que un hilo consumidor la imprime.


## La clase `CountDownLatch`

La clase `CountDownLatch` del paquete `java.util.concurrent` es una utilidad de sincronización que permite que **uno o más threads esperen hasta que otros threads finalicen su trabajo**.

**El funcionamiento** esquemático de `CountDownLatch` o "*cuenta atrás de cierre*" es el siguiente:

* Implementa un punto de espera que denominaremos "*puerta de cierre*", donde uno o más hilos esperan a que otros finalicen su trabajo.

* Los hilos que deben finalizar su trabajo se controlan mediante un contador que llamaremos "*cuenta atrás*".

* Cuando la "*cuenta atrás*" llega a cero se reanudará el trabajo del hilo o hilos interrumpidos y puestos en espera.

* No será posible volver a utilizar la "*cuenta atrás*", es decir, **no se puede reiniciar**. Si fuera necesario reiniciar la "*cuenta atrás*" habrá que pensar en utilizar la clase `CyclicBarrier`.

**Los aspectos más importantes al usar la clase `CountDownLatch`** son los siguientes:

* Al constructor `countDownLatch(int cuenta)` se le indica, mediante el parámetro "cuenta", el total de hilos que deben completar su trabajo, que será el valor de la "*cuenta atrás*".

* El hilo en curso desde el que se invoca al método `await()` esperará en la "*puerta de cierre*" hasta que la "*cuenta atrás*" tome el valor cero. También se puede utilizar el método `await(long tiempoespera, TimeUnit unit)`, para indicar que la espera será hasta que la cuenta atrás llegue a cero o bien se sobrepase el tiempo de espera especificado mediante el parámetro `tiempoespera`.

* La "*cuenta atrás*" se irá decrementando mediante la invocación del método `countDown()`, y cuando ésta llega al valor cero se libera el hilo o hilos que estaban en espera, continuando su ejecución.

* No se puede reinicar o volver a utilizar la "*cuenta atrás*" una vez que ésta toma el valor cero. Si esto fuera necesario, entonces debemos pensar en utilizar la clase `CyclicBarrier`.

* El método `getCount()` obtiene el valor actual de la "*cuenta atrás*" y generalmente se utiliza durante las pruebas y depuración del programa.

En el siguiente [recurso adicional](https://bitbucket.org/eduxunta/edu-java-psp-programacionmultihilo/src/samples-countDownLatch/) puedes ver un ejemplo de cómo utilizar `CountDownLatch` para sumar todos los elementos de una matriz. Cada fila de la matriz es sumada por un hilo. Cuando todos los hilos han finalizado su trabajo, se ejecuta el procedimiento que realiza la suma global.


## La clase `CyclicBarrier`

La clase `CyclicBarrier` del paquete `java.util.concurrent` es una utilidad de sincronización que permite que uno o más threads se esperen hasta que todos ellos finalicen su trabajo.

**El funcionamiento esquemático** de `CyclicBarrier` o "*barrera cíclica*" es el siguiente:

* Implementa un punto de espera que llamaremos "barrera", donde cierto número de hilos esperan a que todos ellos finalicen su trabajo. 

* Finalizado el trabajo de estos hilos, se dispara la ejecución de una determinada acción o bien el hilo interrumpido continúa su trabajo.

* La barrera se llama *cíclica*, porque se puede volver a utilizar después de que los hilos en espera han sido liberados tras finalizar todos su trabajo, y también se puede reiniciar.

**Los aspectos más importantes al usar la clase `CyclicBarrier`** son los siguientes:

* Indicar al constructor `CyclicBarrier(int hilosAcceden)` el total de hilos que van a usar la barrera mediante el parámetro `hilosAcceden`. Este número sirve para disparar la barrera.

* La barrera se dispara cuando llega el último hilo.

* Cuando se dispara la barrera, dependiendo del constructor, `CyclicBarrier(int hilosAcceden)` o `CyclicBarrier(int hilosAcceden, Runnable acciónBarrera)` se lanzará o no una acción, y entonces se liberan los hilos de la barrera. Esa acción puede ser realizada mediante cualquier objeto que implemente `Runnable`.

* El método principal de esta clase es `await()` que se utiliza para indicar que el hilo en curso ha concluido su trabajo y queda a la espera de que lo hagan los demás.

**Otros métodos de esta clase** que puedes utilizar son:

* El método `await(long tiempoespera, TimeUnit unit)` funciona como el anterior, pero en este caso el hilo espera en la barrera hasta que los demás finalicen su trabajo o se supere el `tiempoespera`.

* El método `reset()` permite reiniciar la barrera a su estado inicial.

* El método `getNumberWaiting()` devuelve el número de hilos que están en espera en la barrera.

* El método `getParties()` devuelve el número de hilos requeridos para esa barrera

En el siguiente [recurso adicional](https://bitbucket.org/eduxunta/edu-java-psp-programacionmultihilo/src/samples-CyclicBarrier/) puedes ver un ejemplo parecido al anterior, pero ahora resuelto con `CyclicBarrier`. Cada fila de la matriz ahora representa los valores recaudados por un cobrador. 
Cada fila es sumada por un hilo. Cuando 5 de estos hilos finalizan su trabajo, se dispara un objeto que implementa `Runnable` para obtener la suma recaudada hasta el momento. Como la matriz del ejemplo tiene 10 filas, la suma de sus elementos se hará mediante una barrera de 5 hilos y que se utilizará por tanto de forma cíclica dos veces.

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)