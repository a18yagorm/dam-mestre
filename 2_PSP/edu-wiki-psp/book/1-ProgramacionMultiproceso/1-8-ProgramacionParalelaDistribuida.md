# Programación paralela y distribuida

Dos procesos se ejecutan de forma paralela, si las instrucciones de ambos se están ejecutando realmente de forma simultánea. Esto sucede en la actualidad en sistemas que poseen más de un núcleo de procesamiento.

La programación paralela y distribuida consideran los aspectos conceptuales y físicos de la computación paralela; siempre con el objetivo de mejorar las prestaciones aprovechado la ejecución simultánea de tareas.

Tanto en la **programación paralela como distribuida**, existe ejecución simultánea de tareas que resuelven un problema común. **La diferencia entre ambas es**:

* **La programación paralela** se centra en microprocesadores multinúcleo (en nuestros PC y servidores) o sobre los llamados supercomputadores, fabricados con arquitecturas específicas, compuestos por gran cantidad de equipos idénticos interconectados entre sí y que cuentan son sistemas operativos propios.

* **La programación distribuida** en sistemas formados por un conjunto de ordenadores heterogéneos interconectados entre sí, por redes de comunicaciones de propósito general: redes de área local, metropolitana; incluso a través de Internet. Su gestión se realiza utilizando componentes, protocolos estándar y sistemas operativos de red.


**En la computación paralela y distribuida**:

* Cada procesador tiene asignada la tarea de resolver una porción del problema.
* En programación paralela, los procesos pueden intercambiar datos a través de direcciones de memoria compartidas o mediante una red de interconexión propia.
* En programación distribuida, el intercambio de datos y la sincronización se realizará mediante intercambio de mensajes. 
* El sistema se presenta como una unidad ocultando la realidad de las partes que lo forman.

### Para saber más

Cada 6 meses se publica un [ranking con 500 supercomputadores más potentes del mundo](https://www.top500.org/lists/). En noviembre de 2011 el supercomputador más ponente se encuentra en Japón y posee más de 700.000 núcleos de procesamiento. Curiosamente, en junio de 2005, el computador MareNostrum que se encuentra en Barcelona ocupó el puesto 5 del Top500, con sus nada despreciables 4800 núcleos


## Conceptos básicos

Comencemos revisando algunas clasificaciones de sistemas distribuidos y paralelos:

* En función de los conjuntos de instrucciones y datos (conocida como la taxonomía de Flynn):
	* La arquitectura secuencial la denominaríamos SISD (single instruction single data).
	* Las diferentes arquitecturas paralelas (o distribuidas) en diferentes grupos: SIMD (single instruction multiple data), MISD (multiple instruction single data) y MIMD (multiple instruction multiple data), con algunas variaciones como la SPMD (single program multiple data).
	
* Por comunicación y control:

	* Sistemas de multiprocesamiento simétrico (SMP). Son MIMD con memoria compartida . Los procesadores se comunican a través de esta memoria compartida; este es el caso de los microprocesadores de múltiples núcleos de nuestros PCs.

	* Sistemas MIMD con memoria distribuida . La memoria está distribuida entre los procesadores (o nodos) del sistema, cada uno con su propia memoria local, en la que poseen su propio programa y los datos asociados. Una red de interconexión conecta los procesadores (y sus memorias locales), mediante enlaces (links) de comunicación, usados para el intercambio de mensajes entre los procesadores. Los procesadores intercambian datos entre sus memorias cuando se pide el valor de variables remotas. Tipos específicos de estos sistemas son:

		* **Clusters**. Consisten en una colección de ordenadores (no necesariamente homogéneos) conectados por red para trabajar concurrentemente en tareas del mismo programa. Aunque la interconexión puede no ser dedicada.

		* **Grid**. Es un cluster cuya interconexión se realiza a través de internet.

Veamos una introducción a algunos conceptos básicos que debemos conocer para desarrollar en sistemas distribuidos:

* Distribución: construcción de una aplicación por partes, a cada parte se le asigna un conjunto de responsabilidades dentro del sistema.

* Nudo de la red: uno o varios equipos que se comportan como una unidad de asignación integrada en el sistema distribuido.

* Un objeto distribuido es un módulo de código con plena
autonomía que se puede instanciar en cualquier nudo de la red y a
cuyos servicios pueden acceder clientes ubicados en cualquier otro nudo.

* Componente: Elemento de software que encapsula una serie de funcionalidades. Un componente, es una unidad independiente, que puede ser utilizado en conjunto con otros componentes para formar un sistema más complejo (concebido por ser reutilizable). Tiene especificado: los servicios que ofrece; los requerimientos necesarios para poder ser instalado en un nudo; las posibilidades de configuración que ofrece; y, no está ligado a ninguna aplicación, que se puede instanciar en cualquier nudo y ser gestionado por herramientas automáticas. Sus características:
	* Alta cohesión: todos los elementos de un componente están estrechamente relacionados.
	* Bajo acoplamiento: nivel de independencia que tiene un componente respecto a otros. 

* Transacciones: Conjunto de actividades que se ejecutan en diferentes nudos de una plataforma distribuida para ejecutar una tarea de negocio. Una transacción finaliza cuando todas las partes implicadas, clientes y múltiples servidores confirman que sus correspondientes actividades, han concluido con éxito. **Propiedades ACID de una transacción**:

	* Atomicidad: Una transacción es una unidad indivisible de trabajo, Todas las actividades que comprende deben ser ejecutadas con éxito.
	* Congruencia: Después de que una transacción ha sido ejecutada, la transacción debe dejar el sistema en estado correcto. Si la transacción no puede realizarse con éxito, debe restaurar el sistema al estado original previo al inicio de la transacción.
	* Aislamiento: La transacciones que se ejecutan de forma concurrente no deben tener interferencias entre ellas. La transacción debe sincronizar el acceso a todos los recursos compartidos y garantizar que las actualizaciones concurrentes sean compatibles entre si. 
	* Durabilidad: Los efectos de una transacción son permanentes una vez que la transacción ha finalizado con éxito.

* Gestor de transacciones. Controla y supervisa la ejecución de transacciones, asegurando sus propiedades ACID.

## Tipos de paralelismo

Las mejoras arquitectónicas que han sufrido los computadores se han basado en la obtención de rendimiento explotando los diferentes niveles de paralelismo.

**En un sistema podemos encontrar los siguientes niveles de paralelismo**:

* A nivel de bit. Conseguido incrementando el tamaño de la palabra del microprocesador. Realizar operaciones sobre mayor número de bits. Esto es, el paso de palabras de 8 bits, a 16, a 32 y en los microprocesadores actuales de 64 bits.
* A nivel de instrucciones. Conseguida introduciendo pipeline en la ejecución de instrucciones máquina en el diseño de los microprocesadores.
* A nivel de bucle. Consiste en dividir las interaciones de un bucle en partes que se pueden realizar de manera complementaria. Por ejemplo, un bucle de 0 a 100; puede ser equivalente a dos bucles, uno de 0 a 49 y otro de 50 a 100.
* A nivel de procedimientos. Identificando qué fragmentos de código dentro de un programa pueden ejecutarse de manera simultánea sin interferir la tarea de uno en la de otro.
* A nivel de tareas dentro de un programa. Tareas que cooperan para la solución del programa general (utilizado en sistemas distribuidos y paralelos).
* A nivel de aplicación dentro de un ordenador. Se refiere a los conceptos que vimos al principio de la unidad, propios de la gestión de procesos por parte del sistema operativo multitarea: planificador de procesos, Round-Robin, quamtum, etc.

En sistemas distribuidos hablaremos del **tamaño de grano o granularidad, que es una medida de la cantidad de computación** de un proceso software. Y se considera como el segmento de código escogido para su procesamiento paralelo.

* **Paralelismo de Grano Fino**: No requiere tener mucho
conocimiento del código, la paralelización se obtiene de forma casi automática. Permite obtener buenos resultados en eficiencia en
poco tiempo. Por ejemplo: la descomposición de bucles.

* **Paralelismo de Grano Grueso**: Es una paralelización de alto nivel, que engloba al grano fino. Requiere mayor conocimiento del código, puesto que se paraleliza mayor cantidad de él. Consigue mejores rendimientos, que la paralelización fina, ya que intenta evitar los overhead (exceso de recursos asignados y utilizados) que se suelen producir cuando se divide el problema en secciones muy pequeñas. Un ejemplo de paralelismo grueso lo obtenemos descomponiendo el problema en dominios (dividiendo un conjunto de datos a procesar, acompañando a estos, las acciones que deban realizarse sobre ellos).

### Para saber más


En programación paralela, además de utilizar CPUs, se utilizan GPU (las unidades de procesamiento de las tarjetas gráficas). Las GPU poseen una mayor potencia de cómputo al ser procesadores específicamente diseñados para resolver tareas intensivas de cómputo en tiempo real de direccionamiento de gráficos en 3D de alta resolución. En general, tienen muy buen comportamiento en algoritmos de ordenación rápida de grandes listas de datos, y transformaciones bidimensionales.

[El proyecto de arquitectura de computación paralela con GPUs CUDA](https://developer.nvidia.com/cuda-zone), de NVidia, puede programarse en multitud de lenguajes de programación.

Por ejemplo, las simulaciones de dinámica molecular obtienen mejores resultados en sistemas CUDA.


## Modelos de infraestructura para programación distribuida

Las aplicaciones distribuidas requieren que componentes que se ejecutan en diferentes procesadores se comuniquen entre sí. Los modelos de infraestructura que permiten la implementación de esos componentes, son:

* **Uso de Sockets**: Facilitan la generación dinámica de canales de comunicación. Es actualmente la base de la comunicación. Pero al ser de muy bajo nivel de abstracción, no son adecuados a nivel de aplicación.

* **Remote Procedure Call (RPC)**: Abstrae la comunicación a nivel
de invocación de procedimientos. Es adecuada para programación
estructurada basada en librerías.

* **Invocación remota de objetos**: Abstrae la comunicación a la invocación de métodos de objetos que se encuentran distribuidos por el sistema distribuido. Los objetos se localizan por su identidad. Es adecuada para aplicaciones basadas en el paradigma OO.

* **RMI (Remote Method Invocation)** es la solución Java para la comunicación de objetos Java distribuidos. Presenta un inconveniente, y es que el paso de parámetros por valor implica tiempo para hacer la serialización, enviar los objetos serializados a través de la red y luego volver a recomponer los objetos en el destino.

* **CORBA** (Common Object Request Brocker Architecture) . Para facilitar el diseño de aplicaciones basadas en el paradigma Cliente/Servidor. Define servidores estandarizados a través de un modelo de referencia, los patrones de interacción entre clientes y servidores y las especificaciones de las APIs. 

* **MPI** ("Message Passing Interface", Interfaz de Paso de Mensajes) es un estándar que define la sintaxis y la semántica de las funciones contenidas en una biblioteca de paso de mensajes diseñada para ser usada en programas que exploten la existencia de múltiples procesadores.

* **La Interfaz de Paso de Mensajes** es un protocolo de comunicación entre computadoras. Es el estándar para la comunicación entre los nodos que ejecutan un programa en un sistema de memoria distribuida. Las llamadas de MPI se dividen en cuatro clases:
	* Llamadas utilizadas para inicializar, administrar y finalizar comunicaciones. 
	* Llamadas utilizadas para transferir datos entre un par de procesos. 
	* Llamadas para transferir datos entre varios procesos.
	* Llamadas utilizadas para crear tipos de datos definidos por el usuario.
	
* **Máquina paralela virtual**. Paquetes software que permite ver el conjunto de nodos disponibles como una máquina virtual paralela; ofreciendo una opción práctica, económica y popular hoy en día para aproximarse al cómputo paralelo.


### Para saber más

Existen gran cantidad de proyectos computación distribuida. En el siguiente enlace, puedes ver un [listado de ellos](https://en.wikipedia.org/wiki/List_of_distributed_computing_projects). Por curiosidad, dos se están desarrollando en la Univerdidad Complutense de Madrid. Uno de ellos es una red neuronal que simula el comportamiento de la gran y compleja red de células neuronales autómatas; otro proyecto, utiliza modelos matemáticos de una red social, para estudiar la evolución ideológica de un grupo de personas a través del tiempo.


## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)
