# README

## Para qué es este repo?

Este repo plantea una serie de ejemplos de programación concurrente.

Los ejemplos están distribuidos en varias ramas distintas:

Rama					| Uso
------------ 			| -------------
`master`	 									| Sólo README
`samples-problemaJardines`					| Planteamiento del problema de los jardines
`samples-problemaJardinesSincronizado` 	| Sincronización en el problema de los jardines
`samples-monitoresSynchronized`			| Sencillo ejemplo que simula el acceso simultáneo de 4 terminales a un servidor utilizando monitores Java. 
