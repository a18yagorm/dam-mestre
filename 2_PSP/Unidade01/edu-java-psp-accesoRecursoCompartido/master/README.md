# README

## Para qué es este repo?

Este repo plantea un sencillo ejemplo de acceso a un recurso compartido sin mecanismos de sincronización

Vamos a implementar dos aplicaciones:

Una aplicación que accede a un fichero que contiene un valor entero. Lee ese valor, lo incrementa en 1 y escribe ese valor actualizado en el mismo fichero. Esquemáticamente:

1. `valor = LeerValorFichero(fichero);`
2. `valor++;`
3. `EscribirValorFichero(fichero, valor)`

Una aplicación que crea varios procesos de la anterior. De forma que todos esos procesos utilizarán el mismo fichero para realizar esas senciallas instrucciones.

Veremos cómo probar el ejemplo. ¿El resultado final, es el esperado? ¿Cómo vemos qué es lo que ha pasado?

## Parte 1: Aplicación que modifica el valor de un fichero

En este caso, vamos a recordar como implementar una aplicación sencilla, que:

1. Lee un valor de un fichero.
2. Incrementa ese valor en uno.
3. Escribe ese valor incrementado en el fichero del que lo había leido.

Por supuesto, gestionaremos las posibles excepciones, que se
pueden producir en los accesos a un fichero. Comprobaremos que la ejecución de la aplicación cumple los
resultados que esperamos de ella; y que por lo tanto está
correcta.

Revisa el [código de esta aplicación](src/main/java/psp/parte1/Escritor.java) denominada `Escritor`.

Puedes probarlo directamente con las aplicaciones empaquetadas de la siguiente forma:

```bash
# Sitúate en la raiz del proyecto (donde se encuentra el pom.xml)
java -jar target/escritor
```

## Parte 2: Lanzar la ejecución de varias instancias de la aplicación anterior

Esta segunda aplicación es todavía más sencilla. Simplemente, esta aplicación crea 20 instancias de la primera, de esta forma:

```java
nuevoProceso = Runtime.getRuntime().exec("java -jar " + "escritor " + i + " nuevo.txt");
```

Revisa el [código de esta aplicación](src/main/java/psp/parte2/Lanzador.java) denominada `Lanzador`.

Advierte que intentará lanzar una aplicación que se encuenta en el mismo path desde el que iniciamos esta segunda App que se llama `escritor`.

```bash
# Sitúate en la raiz del proyecto (donde se encuentra el pom.xml)
java -jar target/lanzador
```

## Conclusiones

Reflexiona sobre lo siguiente:

> Si son 20 instancias y cada una incrementa en 1 el valor que hay en el fichero `nuevo.txt`, ¿finalmente el valor que contiene ese fichero será 20? Comprueba la salida y saca conclusiones. ¿Por qué sucede eso?

Si quieres ver una alternativa con mecanismos de sincronización cambiaté a una nueva versión de esta práctica/actividad que está apuntada por la rama `conSincro`.

```
git checkout conSincro
```

### Prueba del código

Se trata de un proyecto maven. Asegúrate que tienes [maven](https://maven.apache.org/install.html) instalado y puedes ejecutarlo en consola (`mvn --version`). Sitúate en la raiz de este repo (en el mismo directorio que se encuentra el fichero `pom.xml`)

Ejecuta la siguiente sentencia en consola:

```bash
mvn clean package
```

Esto generará una carpeta `target/`en el directorio en el que te encuentras y ahí encontrarás la los ficheros jar: `lanzador` y `escritor`.